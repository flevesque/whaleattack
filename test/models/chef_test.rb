require 'test_helper'

class ChefTest < ActiveSupport::TestCase
  def setup
    @chef = Chef.new(chefname: 'chefname', email: 'email@email.email')
  end

  test 'should be valid' do
    assert @chef.valid?
  end

  test 'chefname should be present' do
    @chef.chefname = ''
    assert_not @chef.valid?
  end

  test 'name should be less than 30 characters' do
    @chef.chefname = 'a' * 31
    assert_not @chef.valid?
  end

  test 'email should be present' do
    @chef.email = ''
    assert_not @chef.valid?
  end

  test 'email should be more than 6 characters' do
    @chef.email = 'a' * 5
    assert_not @chef.valid?
  end

  test 'email should be less than 100 characters' do
    @chef.email = 'a' * 101
    assert_not @chef.valid?
  end

  test 'email should accept valid format' do
    valid_emails = ['person@email.dom', 'p.erson@mail.dom', 'per+son@email.co.nz']
    valid_emails.each do |valid_email|
      @chef.email = valid_email
      assert @chef.valid?, "#{valid_email.inspect} should be valid"
    end
  end

  test 'email should reject invalid addresses' do
    valid_emails = ['person@email.', 'p.erson@maildom', 'per+sonemail.co.nz']
    valid_emails.each do |valid_email|
      @chef.email = valid_email
      assert_not @chef.valid?, "#{valid_email.inspect} should be NOT be valid"
    end
  end

  test 'email should be unique' do
    @duplicate_chef = Chef.new(chefname: @chef.chefname, email: @chef.email)
    @chef.save
    assert_not @duplicate_chef.valid?
  end

  test 'email should be case insensitive' do
    @duplicate_chef = Chef.new(chefname: @chef.chefname, email: @chef.email.upcase)
    @chef.save
    assert_not @duplicate_chef.valid?
  end
end